# Spock test example application

## Overview

The application includes the following classes:

- ***Calculator*** - a simple application that performs calculations on integers.
- ***MainController*** - a simple RestController class for a SpringBoot application that displays the typed message in the browser
- ***Application*** - main class for a SpringBoot application

The unit tests in the application were written using the Spock library.

## Run application

In order to start the spring application, the Application class should be started.
If we want to run the calculator, then we should use the Calculator class and its startup method.

## Run test

To run tests in the application, use the `mvn test` or `mvn package` command in the CLI