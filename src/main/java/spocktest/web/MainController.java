package spocktest.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @GetMapping("/")
    public String messageParam(@RequestParam("message") String message) {
        return message;
    }

    @GetMapping("/{message}")
    public String messagePathVariable(@PathVariable("message") String message) {
        return message;
    }
}
