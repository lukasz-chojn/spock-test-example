package spocktest.calculator;

public class Calculator {

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        System.out.println(calculator.add(2, 2));
        System.out.println(calculator.subtract(2, 2));
        System.out.println(calculator.multiply(2, 2));
        System.out.println(calculator.divide(2, 2));
        System.out.println(calculator.divide(2, 0));
    }

    public Integer add(int a, int b) {
        return a + b;
    }

    public Integer subtract(int a, int b) {
        return a - b;
    }

    public Integer multiply(int a, int b) {
        return a * b;
    }

    public Integer divide(int a, int b) {
        if (b == 0) {
            throw new ArithmeticException("You cannot divide by 0");
        }
        return a / b;
    }
}
