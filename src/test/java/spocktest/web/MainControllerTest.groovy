package spocktest.web

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import spock.lang.Specification
import spocktest.web.MainController

@WebMvcTest(controllers = MainController.class)
class MainControllerTest extends Specification {

    @Autowired
    private MockMvc mockMvc

    def "when the method is called with RequestParam, it should return HTTP Status 200 and message text"() {
        given:
        String message = "Hello from RequestParam test"

        when:
        def result = mockMvc.perform(MockMvcRequestBuilders.get("/").param("message", message))

        then:
        result.andExpect(MockMvcResultMatchers.status().isOk())
    }

    def "when the method is called with PathVariable, it should return HTTP Status 200 and message text"() {
        given:
        String message = "Hello from PathVariable test"

        when:
        def result = mockMvc.perform(MockMvcRequestBuilders.get("/{message}", message))

        then:
        result.andExpect(MockMvcResultMatchers.status().isOk())
    }
}
