package spocktest.calculator


import spock.lang.Specification

class CalculatorTest extends Specification {

    def "method should return properly value after adding two numbers"() {
        given:
        def calculator = new Calculator()

        expect:
        calculator.add(a, b) == c

        where:
        a << [5, 10, -20]
        b << [5, 0, -5]
        c << [10, 10, -25]
    }

    def "method should return properly value after subtracting two numbers"() {
        given:
        def calculator = new Calculator()

        expect:
        calculator.subtract(a, b) == c

        where:
        a << [5, 10, -20]
        b << [5, 0, -5]
        c << [0, 10, -15]
    }

    def "method should return properly value after multiplying two numbers"() {
        given:
        def calculator = new Calculator()

        expect:
        calculator.multiply(a, b) == c

        where:
        a << [5, 10, -20]
        b << [5, 0, -5]
        c << [25, 0, 100]
    }

    def "method should return properly value after dividing two numbers"() {
        given:
        def calculator = new Calculator()

        expect:
        calculator.divide(a, b) == c

        where:
        a << [5, 10, -20]
        b << [5, 2, -5]
        c << [1, 5, 4]
    }

    def "method should throw an exception when divide by 0"() {
        given:
        def calculator = new Calculator()
        int a = 2
        int b = 0

        when:
        calculator.divide(a, b)

        then:
        ArithmeticException ex = thrown()
        ex.message == "You cannot divide by 0"
    }
}