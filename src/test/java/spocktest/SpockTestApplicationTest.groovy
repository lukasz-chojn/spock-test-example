package spocktest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification
import spocktest.web.MainController

@SpringBootTest
class SpockTestApplicationTest extends Specification {

    @Autowired
    private MainController webController

    def "when context is loaded then all expected beans are created"() {
        expect: "the WebController is created"
        webController
    }
}
